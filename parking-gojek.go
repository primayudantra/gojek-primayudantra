package main

import (
  "fmt"
  "strconv"
  "io/ioutil"
  "os"
  "bufio"
  "strings"
)

type Registration struct{
  ID int
  Registration string
  Colour string
}

var slots []Registration

/*
  Checking Parking
*/

func checkingParking(){
  fmt.Println("Slot \t No Registration \t No. Colour")
  for _,slot := range slots{
    if slot.Registration != ""{
      fmt.Printf("%d\t %s\t %s \n", slot.ID, slot.Registration, slot.Colour)
    }
  }
  main()
}


/*
  Create Slot Parking
*/
func createSlotparking(slot string){
  s, err := strconv.Atoi(slot)
  checkErr(err)

  slots = make([]Registration, s)
  for i := 0; i < s; i++{
    slots[i] = Registration{ID:i+1,Registration:"",Colour:""}
  }

  fmt.Println("Slot has been submitted ", len(slots))
  main()
}


/*
  Parking Registration
*/
func registrationParking(regisNumber string, colour string){
  if(len(slots) == 0){
    fmt.Println("You have to create parking lot")
  }else{
    for _, slot := range slots{
        if(slot.Registration == regisNumber){
          fmt.Println("Name is exist")
          main()
          return
        }
    }
    for idx, slot := range slots{
      if(slot.Registration == "" && slot.Colour == ""){
        slots[idx].Registration = regisNumber;
        slots[idx].Colour = colour;
        break;
      }
      if(idx == len(slots)-1){
        fmt.Println("Sorry, parking lot is full")
      }
    }
  }
  main()

}

func checkByRequest(data string, status string){
  if status == "colour"{
    for _,item := range slots{
      if data == item.Colour{
        fmt.Println(item.ID)
      }
    }
  }

  if status == "number"{
    for _,item := range slots{
      if data == item.Registration{
        fmt.Println(item.ID)
      }
    }
  }

  if status == "numberRegistration"{
    for _,item := range slots{
      if data == item.Colour{
        fmt.Println(item.Registration)
        break;
      }
    }
    fmt.Println("Not found")
  }

  main()
}

func removeParkinglot(data string){
  i, err := strconv.Atoi(data)
  checkErr(err)

  slots[i-1].Registration = "";
  slots[i-1].Colour = "";
  fmt.Println("Parking deleted at ", i)
  main();
}

func checkErr(err error) {
	if err != nil {
		fmt.Println(err.Error())
		os.Exit(0)
	}
}



func main(){
  opts := os.Args[1:]
  if(len(opts) == 0){
		data := bufio.NewReader(os.Stdin)
		command, _ := data.ReadString('\n')
    command = strings.TrimSpace(command)
    menu(command)
  }else{
    f, e := ioutil.ReadFile(opts[0])
    checkErr(e)
    input := strings.Split(string(f), "\n")
    for _, command := range input {
      menu(command)
    }
  }
}


func menu(data string){

  input := strings.Split(data, " ")
  var command string;
  if len(data) > 1 {
    command = input[0]
  }else{
    command = data
  }
  switch command{
    case "create_parking_lot":
      createSlotparking(input[1])
    case "park":
      registrationParking(input[1],input[2])
    case "status":
      checkingParking()
    case "slot_numbers_for_cars_with_colour":
      checkByRequest(input[1], "colour")
    case "slot_number_for_registration_number":
      checkByRequest(input[1], "number")
    case "registration_numbers_for_cars_with_colour":
      checkByRequest(input[1], "numberRegistration")
    case "leave":
      removeParkinglot(input[1])
    default:
      fmt.Println("Please correct your request")
      main()
  }
}
